package ru.t1.gorodtsova.tm.api.repository;

import ru.t1.gorodtsova.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M findOneById(String Id);

    M findOneByIndex(Integer index);

    M removeOne(M model);

    M removeOneById(String Id);

    M removeOneByIndex(Integer index);

    void removeAll();

    void removeAll(Collection<M> collection);

    boolean existsById(String id);

    int getSize();

}
