package ru.t1.gorodtsova.tm.enumerated;

import ru.t1.gorodtsova.tm.comparator.CreatedComparator;
import ru.t1.gorodtsova.tm.comparator.NameComparator;
import ru.t1.gorodtsova.tm.comparator.StatusComparator;
import ru.t1.gorodtsova.tm.model.Task;

import java.util.Comparator;

public enum TaskSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare),
    BY_DEFAULT("Without sort", null);

    private final String displayName;

    private final Comparator<Task> comparator;

    public static TaskSort toSort(final String value) {
        if (value == null || value.isEmpty()) return BY_DEFAULT;
        for (final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return BY_DEFAULT;
    }

    TaskSort(final String displayName, final Comparator<Task> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator<Task> getComparator() {
        return comparator;
    }

}
