package ru.t1.gorodtsova.tm.service;

import ru.t1.gorodtsova.tm.api.service.IAuthService;
import ru.t1.gorodtsova.tm.api.service.IUserService;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.exception.field.LoginEmptyException;
import ru.t1.gorodtsova.tm.exception.field.PasswordEmptyException;
import ru.t1.gorodtsova.tm.exception.user.AccessDeniedException;
import ru.t1.gorodtsova.tm.exception.user.IncorrectDataException;
import ru.t1.gorodtsova.tm.exception.user.PermissionException;
import ru.t1.gorodtsova.tm.model.User;
import ru.t1.gorodtsova.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final boolean locked = user.isLocked() == null || user.isLocked();
        if (locked) throw new IncorrectDataException();
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
