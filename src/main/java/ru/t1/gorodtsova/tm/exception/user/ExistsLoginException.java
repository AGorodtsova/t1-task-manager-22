package ru.t1.gorodtsova.tm.exception.user;

public final class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Error! Login already exists...");
    }

    public ExistsLoginException(final String login) {
        super("Error! This login '" + login + "' already exists...");
    }

}
