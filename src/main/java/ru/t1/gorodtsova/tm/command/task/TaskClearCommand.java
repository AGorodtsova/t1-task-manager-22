package ru.t1.gorodtsova.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Remove all tasks";

    private final String NAME = "task-clear";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        getTaskService().removeAll(getUserId());
    }

}
