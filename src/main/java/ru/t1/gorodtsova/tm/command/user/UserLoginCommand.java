package ru.t1.gorodtsova.tm.command.user;

import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "User login";

    private final String NAME = "login";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
