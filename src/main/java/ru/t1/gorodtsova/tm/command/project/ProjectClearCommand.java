package ru.t1.gorodtsova.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Remove all projects";

    private final String NAME = "project-clear";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        getProjectService().removeAll(getUserId());
    }

}
