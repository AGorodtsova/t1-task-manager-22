package ru.t1.gorodtsova.tm.command.task;

import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Remove task by index";

    private final String NAME = "task-remove-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().removeOneByIndex(getUserId(), index);
    }

}
