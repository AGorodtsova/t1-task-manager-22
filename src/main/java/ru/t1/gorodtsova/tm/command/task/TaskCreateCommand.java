package ru.t1.gorodtsova.tm.command.task;

import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Create new task";

    private final String NAME = "task-create";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getTaskService().create(getUserId(), name, description);
    }

}
