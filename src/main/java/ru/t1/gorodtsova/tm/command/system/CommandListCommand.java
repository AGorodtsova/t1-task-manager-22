package ru.t1.gorodtsova.tm.command.system;

import ru.t1.gorodtsova.tm.api.model.ICommand;
import ru.t1.gorodtsova.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    private final String ARGUMENT = "-cmd";

    private final String DESCRIPTION = "Show command list";

    private final String NAME = "commands";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
