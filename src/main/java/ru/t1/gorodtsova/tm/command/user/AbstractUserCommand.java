package ru.t1.gorodtsova.tm.command.user;

import ru.t1.gorodtsova.tm.api.service.IAuthService;
import ru.t1.gorodtsova.tm.api.service.IUserService;
import ru.t1.gorodtsova.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Override
    public String getArgument() {
        return null;
    }

}
