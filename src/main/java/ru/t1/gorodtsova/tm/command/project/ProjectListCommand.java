package ru.t1.gorodtsova.tm.command.project;

import ru.t1.gorodtsova.tm.enumerated.ProjectSort;
import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Show list projects";

    private final String NAME = "project-list";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        final String sortType = TerminalUtil.nextLine();
        final ProjectSort sort = ProjectSort.toSort(sortType);
        final List<Project> projects = getProjectService().findAll(getUserId(), sort.getComparator());
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

}
